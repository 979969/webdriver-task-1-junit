package com.example.demo;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PastebinTest {

	private WebDriver driver;
	private PastebinPage pastebinPage;

	@BeforeEach
	public void setUp() {
		driver = new ChromeDriver();
		pastebinPage = new PastebinPage(driver);
	}

	@Test
	public void createNewPaste() {
		pastebinPage.open();
		pastebinPage.fillPasteForm("Hello from WebDriver", "10 Minutes", "helloweb");
		pastebinPage.createPaste();

		String pasteUrl = pastebinPage.getPasteUrl();
		System.out.println("URL of the created paste: " + pasteUrl);

		// Assert paste URL contains pastebin.com
		assertEquals(true, pasteUrl.contains("pastebin.com"));

		// Additional clicks for privacy pop-up (Agree) and footer banner (Close X)
		clickAgree(driver);
		clickCloseBanner(driver);
	}

	@AfterEach
	public void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}

	// Method to click AGREE
	private void clickAgree(WebDriver driver) {
		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(3));

		try {
			WebElement confButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[class=' css-47sehv']")));
			if (confButton.isDisplayed()) {
				confButton.click();
			}
		} catch (Exception ignored) {
			// Handle the case where the "Agree" button doesn't appear within the specified wait time
			System.out.println("Agree button didn't appear within the specified wait time.");
		}
	}

	// Method to click Close footer banner
	private void clickCloseBanner(WebDriver driver) {
		try {
			Thread.sleep(5000); // 5000 milliseconds = 5 seconds
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(5));

		try {
			WebElement banner = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("hideSlideBanner")));
			if (banner.isDisplayed()) {
				banner.click();
			}
		} catch (Exception ignored) {
			// Handle the case where the banner doesn't appear within the specified wait time
			System.out.println("Banner downside didn't appear within the specified wait time.");
		}
	}

}
