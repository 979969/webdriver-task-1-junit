package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PastebinPage {

	private WebDriver driver;

	private final String URL = "https://pastebin.com/";

	private By pasteCodeField = By.id("postform-text");
	private By expirationDropdown = By.id("select2-postform-expiration-container");
	private By pasteNameField = By.id("postform-name");
	private By createButton = By.xpath("//button[@class='btn -big']");

	public PastebinPage(WebDriver driver) {
		this.driver = driver;
	}

	public void open() {
		driver.get(URL);
	}

	public void fillPasteForm(String code, String expiration, String name) {
		WebElement pasteCodeFieldElement = driver.findElement(pasteCodeField);
		pasteCodeFieldElement.sendKeys(code);

		WebElement expirationDropdownElement = driver.findElement(expirationDropdown);
		expirationDropdownElement.click();
		WebElement expirationOption = driver.findElement(By.xpath("//li[text()='" + expiration + "']"));
		expirationOption.click();

		WebElement pasteNameFieldElement = driver.findElement(pasteNameField);
		pasteNameFieldElement.sendKeys(name);
	}

	public void createPaste() {
		WebElement createButtonElement = driver.findElement(createButton);
		createButtonElement.click();
	}

	public String getPasteUrl() {
		return driver.getCurrentUrl();
	}
}
